<?php

namespace Application\Model;

use Zend\Http\Request;
use Zend\Http\Client;
use Zend\Stdlib\Parameters;
use Statickidz\GoogleTranslate;
use Rdehnhardt\ExchangeRate\Exchange;

class ServicesHelper{

   
    private $email = "christianpaez15114@gmail.com";
    private $token = "963fbdd4213b5c52c967";


    public function apiConnect($url, $method,$auth,$json=false,$data=[]){

        //nueva instancia de la clase Request y Client
        $request = new Request();
        $client = new Client();

            if($auth){

                if($json){
                    $authentication = base64_encode($this->email.":".$this->token);
                    $request->getHeaders()->addHeaders(array(
                        'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8',
                        'Authorization' => 'Basic '.$authentication,
                    ));
                    $request->setUri($url);
                    $request->setMethod($method);
                    $request->setContent(json_encode($data));

                    $client->setEncType(Client::ENC_FORMDATA);
                    $response = $client->dispatch($request);
     
                }else{
                    $authentication = base64_encode($this->email.":".$this->token);
                    $request->getHeaders()->addHeaders(array(
                        'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8',
                        'Authorization' => 'Basic '.$authentication,
                    ));
                    $request->setUri($url);
                    $request->setMethod($method);

                    $response = $client->dispatch($request);
                    $data = json_decode($response->getBody(), true);
                    return $data;
                }
            }else{

                $request->getHeaders()->addHeaders(array(
                    'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8'
                ));
                $request->setUri($url);
                $request->setMethod($method);
                $response = $client->dispatch($request);
                $data = json_decode($response->getBody(), true);
                return $data;              
            }

    }     

    public function translate($source, $target, $string){

        $trans = new GoogleTranslate();
        return $trans->translate($source, $target, $string);
    }    

    public function getExchangeRate(){

        $data = $this->apiConnect("http://www.apilayer.net/api/live?access_key=1989faa44150fb5b63e1871558ad2814&format=1&currencies=COP","GET",false);
        
        $exchange = $data["quotes"]["USDCOP"];

        return $exchange;
    }    

    public function alegraToApp(){
          
        //transformamos USD a COP
        $tasa_actualizada = $this->getExchangeRate();
        $list_objects = $this->apiConnect("https://app.alegra.com/api/v1/items/","GET",true);
        $y = 0;

        //armamos un arreglo con la informacion de los productos
        foreach ($list_objects as $key => $value) {
            $precio = round($value["price"][0]["price"]/$tasa_actualizada,2);
            $precio_format = number_format($precio, 2, ".", ".");  
            $articulos[$y]["id"] = $value["id"];
            $articulos[$y]["name"] = $this->translate("es","en",$value["name"]);
            $articulos[$y]["description"] = $this->translate("es","en",$value["description"]);
            $articulos[$y]["price_usd"] = $precio_format;
            $y = $y+1;
        }
        return $articulos;
    }    

    public function alegraToAppEdit($id){
          
        $list_objects = $this->apiConnect("https://app.alegra.com/api/v1/items/".$id,"GET",true);
        $tasa_actualizada = $this->getExchangeRate();

        $precio = round($list_objects["price"][0]["price"]/$tasa_actualizada,2);
        $precio_format = number_format($precio, 2, ".", ".");  
        $resultado["id"] = $list_objects["id"];
        $resultado["name"] = $this->translate("es","en",$list_objects["name"]);
        $resultado["description"] = $this->translate("es","en",$list_objects["description"]);
        $resultado["price_usd"] = $precio_format;
        return $resultado;
    }    

    public function appToAlegra($data,$return){
          
        //transformamos USD a COP
        $tasa_actualizada = $this->getExchangeRate();
        $total_price = ($data["price"] * $tasa_actualizada);
        $data["price"] = $total_price;

        //Traducimos de ingles a español
        $data["name"] = $this->translate("en","es",$data["name"]);
        $data["description"] = $this->translate("en","es",$data["description"]);

        if($return){
            return $data;
        }else{
            $this->apiConnect("https://app.alegra.com/api/v1/items","POST",true,true,$data);
        }
    }    


 
}
?>