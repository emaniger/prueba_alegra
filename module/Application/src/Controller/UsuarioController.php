<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use ArrayObject;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Model\Usuario;
use Application\Model\ServicesHelper;

/*
use Zend\Http\Request;
use Zend\Http\Client;
use Zend\Stdlib\Parameters;
use Statickidz\GoogleTranslate;
use Rdehnhardt\ExchangeRate\Exchange;
*/

class UsuarioController extends AbstractActionController
{
    private $listaUsuario;

    public function __construct()
    {
       $this->listaUsuario = new ArrayObject();

       $this->listaUsuario->append(new Usuario(1,"Andres","Guzman"));
       $this->listaUsuario->append(new Usuario(2,"Carlos","Enrique"));
       $this->listaUsuario->append(new Usuario(3,"Jorge","Gonzales"));
       $this->listaUsuario->append(new Usuario(4,"Ana","Castillo"));
    }     

    public function indexAction()
    {
        //el index action hace un redirect a l metodo listar del controlador
        return $this->redirect()->toRoute('usuario',['action'=>'listar']);
    }    

    public function listarAction()
    {
        return new ViewModel(['listaUsuario'=>$this->listaUsuario, 'titulo'=>'Listado de Usuarios']);
    }    

    public function verAction()
    {
        $id = (int) $this->params()->fromRoute("id",0);

        $resultado = null;

        foreach ($this->listaUsuario as $usuario) {
            if($usuario->getId()==$id){
                $resultado = $usuario;
                break;
            }
        }

        return new ViewModel(['usuario'=>$resultado,'titulo'=>'Detalles de Usuario']);
    }    

   
   public function restAction(){

        //instanciamos la clase ServicesHelper para acceder a su metodos auxiliares
        $services = new ServicesHelper();

        //hacemos uso del metodo apiConnect para poder consumir cualquier api, le pasamos la url, el metodo que se enviara y si tiene autenticacion o no

        $data = $services->apiConnect("http://www.apilayer.net/api/live?access_key=1989faa44150fb5b63e1871558ad2814&format=1&currencies=COP","GET",false);
        
        $tasa_actualizada = $data["quotes"]["USDCOP"];
       

        $list_objects = $services->apiConnect("https://app.alegra.com/api/v1/items/","GET",true);
        $y = 0;

        //armamos un arreglo con la informacion de los productos
        foreach ($list_objects as $key => $value) {
            $precio = round($value["price"][0]["price"]/$tasa_actualizada,2);
            $precio_format = number_format($precio, 2, ".", ".");  
            $articulos[$y]["name"] = $services->translate("es","en",$value["name"]);
            $articulos[$y]["description"] = $services->translate("es","en",$value["description"]);
            $articulos[$y]["category"] = $services->translate("es","en",$value["category"]["name"]);
            $articulos[$y]["price_usd"] = $precio_format;
            $y = $y+1;
        }
        
            echo "<pre>";
            print_r($articulos);
            echo "</pre>";
        die();
    }
}
