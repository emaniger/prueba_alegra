<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use ArrayObject;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Model\Usuario;
use Application\Model\ServicesHelper;
use Zend\Mvc\Plugin\FlashMessenger;

class ProductoController extends AbstractActionController
{
    private $articulos;

    public function __construct()
    {
    }     

    public function indexAction()
    {
        //el index action hace un redirect a l metodo listar del controlador
        return $this->redirect()->toRoute('producto',['action'=>'listar']);
    }    

    public function listarAction()
    {
        //instanciamos la clase ServicesHelper para acceder a su metodos auxiliares
        $services = new ServicesHelper();

        $list_objects = $services->alegraToApp();

        return new ViewModel(['articulos'=>$list_objects, 'titulo'=>'Product List']);
    }    


    public function nuevoAction(){
         
        $services = new ServicesHelper();

        if($this->getRequest()->isPost()) {

            $data = $this->params()->fromPost();  
            $services->appToAlegra($data,false);

            return $this->redirect()->toRoute('producto',['action'=>'listar']);
        }
    }

    public function editarAction()
    {

        $services = new ServicesHelper();

        $id = (int) $this->params()->fromRoute("id",0);
        $resultado = null;
        

         if($this->getRequest()->isPost()) {

          $info = $this->params()->fromPost();  
          $data = $services->appToAlegra($info,true);
          $data["price"] = ["price"=>$data["price"]];

          $services->apiConnect("https://app.alegra.com/api/v1/items/".$id,"PUT",true,true,$data);

          return $this->redirect()->toRoute('producto',['action'=>'listar']);
      }else{

        $resultado = $services->alegraToAppEdit($id);

        return new ViewModel(['product'=>$resultado,'titulo'=>'Modify Product:']);
      }

        
    }    


/*   
    public function restAction(){

        //instanciamos la clase ServicesHelper para acceder a su metodos auxiliares
        $services = new ServicesHelper();

        //hacemos uso del metodo apiConnect para poder consumir cualquier api, le pasamos la url, el metodo que se enviara y si tiene autenticacion o no

        $data = $services->apiConnect("http://www.apilayer.net/api/live?access_key=1989faa44150fb5b63e1871558ad2814&format=1&currencies=COP","GET",false);
        
        $tasa_actualizada = $data["quotes"]["USDCOP"];
       

        $list_objects = $services->apiConnect("https://app.alegra.com/api/v1/items/","GET",true);
        $y = 0;

        //armamos un arreglo con la informacion de los productos
        foreach ($list_objects as $key => $value) {
            $precio = round($value["price"][0]["price"]/$tasa_actualizada,2);
            $precio_format = number_format($precio, 2, ".", ".");  
            $articulos[$y]["name"] = $services->translate("es","en",$value["name"]);
            $articulos[$y]["description"] = $services->translate("es","en",$value["description"]);
            $articulos[$y]["category"] = $services->translate("es","en",$value["category"]["name"]);
            $articulos[$y]["price_usd"] = $precio_format;
            $y = $y+1;
        }
        
            echo "<pre>";
            print_r($articulos);
            echo "</pre>";
        die();
    }
    */
}
